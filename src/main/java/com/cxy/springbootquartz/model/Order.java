package com.cxy.springbootquartz.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {
    private Integer id;

    private BigDecimal amount;

    private Integer status;

    private String receiveName;

    private String receiveAddress;

    private String receiveMobile;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;


}