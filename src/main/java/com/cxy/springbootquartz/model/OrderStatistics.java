package com.cxy.springbootquartz.model;

import lombok.Data;

import java.util.Date;

@Data
public class OrderStatistics {
    private Integer id;

    private Date time;

    private Integer orderCount;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

}