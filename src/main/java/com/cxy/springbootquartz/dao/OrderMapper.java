package com.cxy.springbootquartz.dao;

import com.cxy.springbootquartz.model.Order;
import com.cxy.springbootquartz.model.OrderExample;

import java.util.Date;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

public interface OrderMapper {
    long countByExample(OrderExample example);

    int deleteByExample(OrderExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    List<Order> selectByExample(OrderExample example);

    Order selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example);

    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    @Select("select * from t_order where create_time > #{startDate} <= #{endDate} ")
    @ResultMap("BaseResultMap")
    List<Order> selectByDate(@Param("startDate") Date startDate,@Param("endDate") Date endDate);
}