package com.cxy.springbootquartz.dao;

import com.cxy.springbootquartz.model.OrderStatistics;
import com.cxy.springbootquartz.model.OrderStatisticsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderStatisticsMapper {
    long countByExample(OrderStatisticsExample example);

    int deleteByExample(OrderStatisticsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(OrderStatistics record);

    int insertSelective(OrderStatistics record);

    List<OrderStatistics> selectByExample(OrderStatisticsExample example);

    OrderStatistics selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") OrderStatistics record, @Param("example") OrderStatisticsExample example);

    int updateByExample(@Param("record") OrderStatistics record, @Param("example") OrderStatisticsExample example);

    int updateByPrimaryKeySelective(OrderStatistics record);

    int updateByPrimaryKey(OrderStatistics record);
}