package com.cxy.springbootquartz.listener;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.listeners.TriggerListenerSupport;

import java.time.LocalTime;

/**
 * @author: huayushu luming
 * @date: 2020-11-07 16:03
 * @desc: springboot 暂不支持Listener
 **/
//@Slf4j
//public class MyTriggerListener extends TriggerListenerSupport {
//    @Override
//    public String getName() {
//        return "myTriggerListener";
//    }
//
//    @Override
//    public void triggerFired(Trigger trigger, JobExecutionContext context) {
//        super.triggerFired(trigger, context);
//        LocalTime noew = LocalTime.now();
//        log.info(noew.toString()+"Trigger is fired!");
//    }
//
//    @Override
//    public void triggerComplete(Trigger trigger, JobExecutionContext context, Trigger.CompletedExecutionInstruction triggerInstructionCode) {
//        super.triggerComplete(trigger, context, triggerInstructionCode);
//        String code = triggerInstructionCode.toString();
//        LocalTime now = LocalTime.now();
//        log.info(now.toString()+"Trigger is completed!");
//    }
//}
