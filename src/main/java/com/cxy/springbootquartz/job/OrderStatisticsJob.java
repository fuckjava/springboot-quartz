package com.cxy.springbootquartz.job;

import com.cxy.springbootquartz.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Calendar;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author: huayushu luming
 * @date: 2020-11-07 18:37
 * @desc:
 **/
@Slf4j
public class OrderStatisticsJob extends QuartzJobBean {
    @Autowired
    private OrderService orderService;
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
      log.info("订单统计执行");
      orderService.orderStatistics();
    }
}
