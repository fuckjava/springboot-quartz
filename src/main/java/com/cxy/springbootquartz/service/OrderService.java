package com.cxy.springbootquartz.service;

import com.cxy.springbootquartz.dao.OrderMapper;
import com.cxy.springbootquartz.dao.OrderStatisticsMapper;
import com.cxy.springbootquartz.model.Order;
import com.cxy.springbootquartz.model.OrderStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author: huayushu luming
 * @date: 2020-11-07 18:16
 * @desc:
 **/
@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderStatisticsMapper orderStatisticsMapper;

    public void createOrder(){
        Random random=new Random();
        int count = random.nextInt(5);
        for (int i = 0; i <count ; i++) {
            Order order=new Order();
            order.setAmount(BigDecimal.TEN);
            order.setReceiveName("xxxx");
            order.setReceiveAddress("上海市静安区");
            order.setReceiveMobile("13662240488");
            order.setCreateTime(new Date());
            order.setCreateUser("test");
            order.setUpdateTime(new Date());
            order.setUpdateUser("test");
            orderMapper.insertSelective(order);
        }
    }

    public void orderStatistics() {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND,0);
        now.set(Calendar.MILLISECOND,0);

        Calendar startDate = Calendar.getInstance();
        startDate.set(Calendar.SECOND,0);
        startDate.set(Calendar.MILLISECOND,0);
        startDate.add(Calendar.MINUTE,-1);

        List<Order>orders=orderMapper.selectByDate(startDate.getTime(),now.getTime());

        OrderStatistics os = new OrderStatistics();
        os.setTime(now.getTime());
        os.setOrderCount(orders.size());
        os.setCreateTime(new Date());
        os.setUpdateTime(new Date());
        os.setCreateUser("system");
        os.setUpdateUser("system");
        orderStatisticsMapper.insertSelective(os);



    }
}
